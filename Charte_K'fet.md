# Charte K'fet BDE Info 

## Le règlement 

1. Toute personne dans le service devra vérifier le bon fonctionnement des équipements en cas de non/mal-fonctionnement il faut le signaler le plus rapidement possible au RP K'fet

2. Aucun membre du BDE n'a de passe-droit à la k'fet. Les seuls passe-droit accordés par la k'fet sont: 

   1. Le menu gratuit pour avoir fait les courses
   2. Le repas gratuit pour avoir participé à un service

3. Les postes devront être nettoyés et rangés après chaque service. *cf: Les postes* 

4. La vaisselle devra être faite après tout service réalisé

5. Les poubelles devront être jetées tous les deux jours et avant le week-end, jours fériés et vacances

6. Les machines à paninis devront être nettoyées dès l'annonce de la fin de service 

7. Les micro-ondes devront être nettoyés toutes les semaines

8. Le sol de la k'fet devra être nettoyé toutes les semaines

9. Les stocks devront être vérifiés à 10h avant le début du service (*Date de péremption,état du produit, stock*)

10. Les gants, charlottes et masques devront être portés lors des services

    ---------------------------------------

    ## Les postes 

    ### Commis

    #### Travail à 10h

    ##### Commis 1:

    * Vente au comptoir en Lyfpay et espèces (*pain choc/croissant, boissons et desserts*)

    ##### Commis 2:

    * Sortir les paninis et prévenir le PC (*x20 ou x10 le jeudi*)	

    * Mettre les boissons au frais:		

      * IceTea x5
      * Coca x4
      * 7Up x3	
      * Pcf x2
      * Tropical x2
      * Schwepps x2		
      * Orange x2 (Bac du bas)
      * Pomme x2 (Bac du bas)	

    * Vérifier les stocks de desserts, de compotes et de salade et prévenir le PC	

    * Vérifier les stocks des sandwichs (Assiettes, gobelets et sachets)

    * Sortir 9 verres numérotés

      > tip: gardez toujours vos commandes dans l’ordre

    * Récupérer la spatule, la pince, le ciseau et un marqueur

    * Sortir le Nutella

    #### Travail à 12h

    ##### Commis 1:

    * Brancher les machines

    * Couper les paninis au ciseau

    * Faire cuire les paninis dans la machine n°3 (3 max, mieux vaut attendre)		

      > tip: fromage moelleux ou pain craquant, c’est prêt !

    * Faire cuire les croqs et wraps dans la machine n°2 dans du papier cuisson !

      > tip: fromage fondu, c’est prêt ! (pensez à soulever régulièrement le papier vous galèrerez moins à le sortir)

    * Réaliser les paninis nut (2 spatules) et wraps nut (1 spatule)

    * Répondre aux demandes de son partenaire en lui apportant boissons, desserts, verres ou tout autre de ses besoins

    ##### Commis 2:

    * Sortir une boisson de chaque

    * Lire l’écran des sandwichs

      > tip: procédez toujours dans l’ordre des commandes, pas de folie

    * Numéroter et remplir le verre jusqu'au dernier trait

      > tip: pour les boissons surprises finissez vos fonds de bouteille
      > tip: pour les boissons gazeuses inclinez le verre
      > tip: mettez un X sur celles qui n’ont pas de dessert et P ou W sur celles qui attendent un panini ou wrap nut vous regarderez moins l’écran 

    * Sortir le dessert correspondant

      * Si ce n’est pas une confiserie demandez à votre partenaire de vous le sortir et numérotez une assiette. 

      * Mettre le tout au micro-onde:

        * Donuts 1m00s

        * Beignets 1m10s

        * Muffins 1m30s

        * Pour chauffer plusieurs desserts réglez le temps du dessert le plus long et ajouter une minute par dessert

          > tip: notez le goût du beignet sur l’assiette une erreur est vite arrivée
          > tip: posez le dessert sur le verre correspondant, gagnez de la place
          > tip: passez commande à votre partenaire par 10 commandes (annoncez lui aussi les wraps et paninis nut)	

    * Apporter les commandes au comptoir en criant le numéro de commande (*pensez aux couverts*)

    * Transmettre les paninis et croqs crus à votre partenaire

    #### Travail post-service

    ##### Commis 1:

    * Débrancher les machines puis les nettoyer

    * Mettre la pince et les ciseau à l’évier

    * Se faire à manger et manger

      > tip: les sandwichs finissent bien avant vous, passez leur votre commande.

    ##### Commis 2:	

    * Fermer les bouteilles et le nutella

    * Mettre la spatule à l’évier

    * Se faire à manger et manger

      > tip: les sandwichs finissent bien avant vous, passez leur votre commande.

    #### Travail à 16h

    ##### Commis 1:

    * Nettoyer le plan de travail avec une éponge humide
    * Jeter les bouteilles dans une poubelle vide sur le parking de l’IUT

    ##### Commis 2:	

    * Ranger les bouteilles et le Nutella
    * Nettoyer le plan de travail avec une éponge humide
    * Nettoyer le micro-onde
    * Remplir les stocks de confiseries
    * Remplir les stocks des sandwichs
    * Faire l’inventaire et prévenir le PC des manques à venir

    ------------------------------

    ### Les Sandwichs 

    #### Travail à 10h

    * Vérifier l'état de propreté de la K’fet et faire la vaisselle en cas de saleté 
    * Se laver les mains et mettre des gants
    * Pré-découper les ingrédients (ne pas hésiter à voir un peu plus que le strict nécessaire pour éviter les rush découpe en plein service)
      * Tomates en rondelle 
      * Emmental à la mandoline *puis le recouper en 3*
      * Jambon *en 5*
      * Dinde *en 4*
      * Raclette au couteau (*en tranche de 3cm de large*)
      * Brie au couteau (*en tranche de 1cm de large*)
      * La mozzarella en rondelle 
    * Mettre tous les ingrédients prédécoupés dans des boîtes au frais 
      * S'il n'y a pas assez de bouchons mettre du cellophane sur la boîte
    * Passer un dernier coup d'éponge pour nettoyer avant le service
    * Vérifier s'il reste des assiettes, verres, papier à paninis et à sandwich 

    #### Travail à 12h

    * Se laver les mains, mettre les gants, la charlotte, et le masque *en cas de maladie*

    * Sortir les ingrédients du frigo : 

      * Les boîtes préparées à 10h 
      * Thon
      * Saucisses 
      * Jambon cru 
      * Rosette
      * Chèvres
      * Sauces 
      * Beurre
      * Cornichons
      * Mais
      * PDM 
      * Wrap

    * Préparer les sandwichs (PDM),les hot-dogs, les paninis, les wraps et assiettes garnies (*être bien attentif*)

      > La personne de gauche vous passera ce qu'il reste dans le frigo

    * Tous les ingrédients vont par trois **sauf** le brie (*2*), le jambon cru (*2*)

      > En cas ingrédient inferieur à 2 on rajoutera un ingrédient de chaque. 

    * Passer les commandes avec le pavé 

      > demander à celui de droite de le faire avec le clavier
      >
      > *demander le focus à l’ordi si jamais ca marche pas*

    * Récupérer les commandes des étudiants pas contents pour compléter le nécessaire (*demander à l’ordi de faire un post it*)

    * S'il manque un ingrédient demander au commis de prévenir le PC

    #### Travail post-service

    * Faire à manger pour tout le monde
    * Se mettre d’accord sur qui fait quoi: 
      * Soit nettoyage du plan de travail, ranger les ingrédients (*en les filmant dans du cellophane*), les sauces et vider les poubelles (*quitte a mettre 2 poubelles ensemble pour en vider une complète*)
      * Soit faire la vaisselle (*celle de toute la journée y compris celle des commis*) bien penser à nettoyer l'évier et l'on évite vaisselle propre dans un bac sale
      * Soit vider les cartons à côté du frigo, et passer le balais dans la k'fet
    * Le rangement des sauces et ingrédients emballés doit être fait avant 14h
    * Les restes de salade ouverte sont jetés
    * Prévenir l’ordi si un ingrédient est sur le point de manquer

    ------------------------------------------------------------

    ### Le PC 

    #### Travail à 10h

    * Rentrer les gens sur le logiciel en fonction du poste de chacun (*ne surtout pas prendre de commande avant*)
    * Compter les baguettes et demander au service s'ils veulent un sandwich à 12h 
    * Mettre le tout dans le logiciel
    * Prendre les commandes de ceux qui peuvent commander à 10h (*soutien, PTUT, licence, etc.*)
    * Nettoyer le comptoir

    #### Travail à 12h

    * Être à l’heure ! (*les gens sont vite impatients*)
    * Ne pas prendre de commande avant 12h
    * Prendre les commandes rapidement dès le début pour éviter une queue trop longue 
    * Être agréable en prenant les commandes (*SBAM - Sourire, Bonjour, Au revoir, Merci*)
    * Bien penser à donner le numéro de commande
    * Expliquer le fonctionnement de la K’fet en 10 secondes (*si un exté ou un piou vient commander pour la 1ère fois*)
    * Faire attention aux stocks faits le matin (*utiliser le tableau*)
    * Rester debout en prenant les commandes (*plus pratique et mieux pour l’image*)
    * Garder le focus sur l’écran du logiciel (*si c’est pour la musique pensez bien que service>musique*)
    * Bien noter les commandes qui sortent 
    * Essayer de gérer les problèmes du service:
      * Pas assez de sauce 
      * Post-it et retour en confection 
      * Problème de boisson ou dessert 
      * Si vol de commande ou autre problème “grave” → en référer au gérant du service 

    #### Travail post-service

    * Nettoyer le comptoir
    * **Nouveauté** faire les stocks pour le lendemain : regarder quels ingrédients manquent 
    * Demander aux confections et commis ce qu’il reste en petite quantité

    ---------------------------------------------

    ## L'hygiène 

    * La k'fet doit être propre avant et après chaque service. 
    * Un grand nettoyage doit être fait entre chaque semestre 
      * Vitre 
      * Frigo
      * Micro-ondes 
    * Le sol de la k'fet doit être nettoyé une fois par semaine (jeudi ou vendredi) suivant le planning. 
    * Les masques, charlottes, et gants ne sont pas optionnels. 
    * Tout comme le nettoyage du plan de travail après le services

    ------------------------------------------------------------

    ## Les courses

    ### Prérequis 

    * La personne effectuant les courses doit être choisie avant le mardi soir maximum. 
    * Deux personnes minimums sont nécessaires pour faire les courses
    * La carte Metro, la liste de course(*Dans le dossier k'fet/courses*), l'argent devront bien être donnés

    ### Pendant les courses

    * Les dates de péremption doivent être regardées avant de les acheter 
    * Ainsi que la fraîcheur des produits (*ex: salade, tomate*) 
    * La k'fet n'empêche pas les "coursiers" de faire leurs propres course, il leur sera seulement demandé de faire une facture à part et de payer par eux-mêmes

    ### Rangement 

    * Lorsque vous déchargez la voiture vous devez prendre: 
      1. Les surgelés (*beignets, muffins, donuts,paninis*) 
      2. Le frais (*Fromages, charcuteries, beurre,compotes*)
      3. Les produits alimentaires restant (*Sauces, boissons, wraps, pain à croque*)
      4. Les produits d'hygiène (*nettoyant, gants, masques, charlotte* ) 
    * Dans le frigo les produits devront être rangés comme suit: 
      * 1er étage: Produits ouverts
      * 2eme étage: Fromages (*Chèvre, Brie, Raclette, Mozza, Emmental*)
      * 3eme étage: Charcuteries (*Rosette, Jambon, Saucisse, Jambon cru, Dinde*)
      * 4eme étage: Sauces, beurres, cornichons
    * Dans le congélateur les produits devront être rangés comme suit: 
      * En bas de gauche à droite: 
        * Beignet chocolat
        * Beignet framboise
        * Beignet pomme 
      * En haut de gauche à droite: 
        * Muffin
        * Donut chocolat
        * Donut sucre 
    * Les paninis devront être rangés dans le petit congélateur sans le carton.
    * Les compotes seront mises dans le frigo commis au dernier étage. 

    ------------------------------------------

    ## Les poubelles 

    ### Dans la k'fet: 

    Les poubelles dans le k'fet: 

    * La poubelle à couvercle rouge: Les bouteilles, elles doivent être pliées pour permettre d'en faire tenir le plus possible. 
    * Le seau rouge pour les bouchons (*pas pour vous embêter mais ça permet de faire des fauteuils roulants pour les handicapés c'est une bonne action*)
    * La panière pour les bouteilles en verre, et flacons en verre 
    * La poubelle à côté de l'évier pour tous les déchets ménagers (*c'est à dire non recyclables*). 
    * Le carton se trouve à côté du frigo commis 

    ### A l'extérieur

    Les poubelles à l'extérieur: 

    * Le cartons et le les déchets ménagers se trouvent sur le parking à gauche du bâtiment informatique 
    * Le plastique se met dans une poubelle vide du dit parking
    * Le verre ?

-----------------------------------------------------------

## Tableau des allergènes

- **Oeuf**: Oeufs et produits à base d’oeufs. 
  - Wrap,Mayonnaise,pain de mie
- **Fruit à coque** Amandes, noisettes, noix, noix de : cajou, pécan, macadamia, du Brésil, du Queensland, pistaches.
- **Gluten** (blé, seigle, orge, avoine, épeautre, kamut ou leurs souches hybridées) et produits à base de ces céréales.
  - Wrap,pain de mie....
- **Poisson** Poissons et produits à base de poissons.
  - Thon
- **Sésame** Graines de sésame et produits à base de graines de sésame.
- **Arachide** Arachides et produits à base d’arachide.
  - Nutella
- **Lait** Lait et produits à base de lait (y compris de lactose).
  - 
- **Sulfites** Anhydride sulfureux et sulfites en concentration de plus de 10mg/kg ou 10 mg/l (exprimés en SO2).
- **Moutarde** Moutarde et produits à base de moutarde.
  - Moutarde
